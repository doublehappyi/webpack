/**
 * Created by db on 16/1/30.
 */
module.exports = {
    entry:"./entry.js",
    output:{
        path:__dirname,
        filename:'bundle.js'
    },
    module:{
        loaders:[{
            test:/\.css$/, loader:"style!css"
        }]
    }
}